package com;


import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class AppointmentHelper  {

    List<Appointment> appointments = new LinkedList<>();


    //Create an appointment list
    public void CreateAppointment () {

        Appointment a1 = new Appointment("Ally",23);
        Appointment a2 = new Appointment("Sal",21);
        Appointment a3 = new Appointment("Murray", 45);
        Appointment a4 = new Appointment("Joe",89);

        appointments.add(a1);
        appointments.add(a2);
        appointments.add(a3);
        appointments.add(a4);
//        System.out.println("LinkedList content: " + appointments);


    }

    //Read all the elements of the list using the iterator
    public void ReadAppointment() {
        System.out.println("LinkedList content: \n" + appointments.toString());

        ListIterator<Appointment> appointmentListIterator = appointments.listIterator();
        System.out.println("All the appointments in the list are :");
        while (appointmentListIterator.hasNext()) {
            System.out.println(appointmentListIterator.next());


        }
    }

    public void UpdateAppointment (){
        Appointment a = new Appointment("andy", 45);
        appointments.set(2,a);

        ListIterator<Appointment> appointmentListIterator = appointments.listIterator();
        System.out.println("Before after updation in the list are :");
        while (appointmentListIterator.hasNext()) {
            System.out.println(appointmentListIterator.next());
        }

    }


    //Delete an appointment at a particular index
    public void DeleteAppointment(){

        appointments.remove(0);
        System.out.println("Elements in the list after deleting are :");

        ListIterator<Appointment> appointmentListIterator = appointments.listIterator();
        while (appointmentListIterator.hasNext()){
            System.out.println( appointmentListIterator.next());
        }
    }



}
