package com;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;


public class Appointment {
    private String patientName;
    private int pId;
      Date date = new Date();
//    SimpleDateFormat date = new SimpleDateFormat("yyyy MMM dd");
    public Appointment() {
        this.patientName = null;
        this.pId = 0;
//        this.date = null;
    }

    public Appointment(String patientName, int pId  ) {
        this.patientName = patientName;
        this.pId = pId;


    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public int getpId() {
        return pId;
    }

    public void setpId(int appointmentNumber) {
        this.pId = appointmentNumber;
    }


    @Override
    public String toString() {
        return "Appointment{" +
                "patientName='" + patientName + '\'' +
                ", pId=" + pId +
                ",date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Appointment that = (Appointment) o;

        if (pId != that.pId) return false;
        return Objects.equals(patientName,pId);
    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(patientName,pId  );
//    }
}
